﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using RESTMaster.Models;
using RESTMaster.Services;
using RESTMaster.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RESTMaster.ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {
        public DelegateCommand PullToRefreshCommand { get; set; }
        public DelegateCommand<PHPost> ItemTappedCommand { get; set; }

        private bool _isRequestProcessing;
        public bool IsRequestProcessing
        {
            get { return _isRequestProcessing; }
            set { SetProperty(ref _isRequestProcessing, value); }
        }

        private ObservableCollection<PHPost> _posts;
        public ObservableCollection<PHPost> Posts
        {
            get { return _posts; }
            set { SetProperty(ref _posts, value); }
        }

        public MainPageViewModel(INavigationService navigationService)
            : base(navigationService)
        {
            Title = "Posts";
            PullToRefreshCommand = new DelegateCommand(OnPullToRefresh);
            ItemTappedCommand = new DelegateCommand<PHPost>(OnItemTapped);

            LoadJSONPlaceHolderPostsAsync();
        }

        private async void OnPullToRefresh()
        {
            Debug.WriteLine($"\n**** {this.GetType().Name}.{nameof(OnPullToRefresh)}");
            await LoadJSONPlaceHolderPostsAsync();
        }

        private async Task LoadJSONPlaceHolderPostsAsync()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(LoadJSONPlaceHolderPostsAsync)}");
            IsRequestProcessing = true;
            var jsonPlaceHolderApiService = new JSONPlaceHolderApiService();
            Posts = await jsonPlaceHolderApiService.GetJSONPlaceHolderPostsAsync();
            IsRequestProcessing = false;
        }

        private void OnItemTapped(PHPost postThatWasTapped)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnItemTapped)}:  {postThatWasTapped.Title}");

            var navParams = new NavigationParameters();
            navParams.Add(NavigationParameterKeys.PostKey, postThatWasTapped);
            _navigationService.NavigateAsync(nameof(PostDetailsPage), navParams, false, true);
        }
    }
}

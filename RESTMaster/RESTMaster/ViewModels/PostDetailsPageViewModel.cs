﻿using System;
using Prism.Navigation;
using RESTMaster.Models;

namespace RESTMaster.ViewModels
{
    public class PostDetailsPageViewModel : ViewModelBase
    {
        private PHPost _post;
        public PHPost Post
        {
            get { return _post; }
            set { SetProperty(ref _post, value); }
        }

        public PostDetailsPageViewModel(INavigationService navigationService)
            : base(navigationService)
        {
            Title = "Post Details";
        }

        public override void OnNavigatingTo(NavigationParameters parameters)
        {
            base.OnNavigatingTo(parameters);
            if (parameters.ContainsKey(NavigationParameterKeys.PostKey))
            {
                Post = parameters[NavigationParameterKeys.PostKey] as PHPost;
            }
        }
    }
}

﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Plugin.Connectivity;
using RESTMaster.Models;

namespace RESTMaster.Services
{
    public class JSONPlaceHolderApiService
    {
        /// <summary>
        /// Print out all json coming back from server if true.
        /// Print just the number of items if false.
        /// </summary>
		private bool _useVerboseOutput = false;

        private string _baseUrl = "http://jsonplaceholder.typicode.com";  // Change this to https to watch Android throw an exception!
        private HttpClient _httpClient;

        public JSONPlaceHolderApiService()
        {
            _httpClient = new HttpClient();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            _httpClient.Timeout = TimeSpan.FromSeconds(5);
        }

        public bool DoIHaveInternet()
        {
            if (!CrossConnectivity.IsSupported)
            {
                Debug.WriteLine($"**** {this.GetType().Name}.{nameof(DoIHaveInternet)}:  Not supported... Returning true no matter what.");
                return true;
            }

            bool hasNet = CrossConnectivity.Current.IsConnected;
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(DoIHaveInternet)}:  returning {hasNet}");
            return hasNet;
        }

        public async Task<ObservableCollection<PHPost>> GetJSONPlaceHolderPostsAsync()
        {
            if (!DoIHaveInternet())
                return null;

            var request = new HttpRequestMessage();
            request.Method = HttpMethod.Get;
            request.RequestUri = new Uri($"{_baseUrl}/posts");

            var content = new StringContent(string.Empty);
            ObservableCollection<PHPost> posts = null;

            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(GetJSONPlaceHolderPostsAsync)}:  Sending request={request.RequestUri.ToString()}");

            try
            {
                // Since HttpResponseMessage is IDisposable, we put it in a "using" block to be automatically disposed.
                using (HttpResponseMessage httpResponseMessage = await _httpClient.SendAsync(request))
                {
                    if (httpResponseMessage.IsSuccessStatusCode == false)
                    {
                        Debug.WriteLine($"**** {this.GetType().Name}.{nameof(GetJSONPlaceHolderPostsAsync)}:  Request failed. StatusCode={httpResponseMessage.StatusCode}");
                        return null;
                    }

                    string responseAsString = await httpResponseMessage.Content.ReadAsStringAsync();
                    posts = JsonConvert.DeserializeObject<ObservableCollection<PHPost>>(responseAsString);

                    var debugComment = _useVerboseOutput
                        ? $"Request succeeded!\n{responseAsString}"
                        : $"Request succeeded! We've got {posts.Count} posts!";

                    Debug.WriteLine($"**** {this.GetType().Name}.{nameof(GetJSONPlaceHolderPostsAsync)}:  {debugComment}");
                } // our HttpResponseMessage gets disposed here.
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"**** {this.GetType().Name}.{nameof(GetJSONPlaceHolderPostsAsync)}:  EXCEPTION:  {ex}");
            }

            return posts;
        }
    }
}

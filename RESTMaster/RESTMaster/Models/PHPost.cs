﻿using System;
using Newtonsoft.Json;

namespace RESTMaster.Models
{
    public class PHPost
    {
        [JsonProperty("userId")] // these JsonProperty attributes must match the property names we get back from the REST service.
        public int UserId { get; set; }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("body")]
        public string Body { get; set; }
    }
}
